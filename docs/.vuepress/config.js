module.exports = {
  title: 'e-Chauffeur docs',
  base: '/e-chauffeur/e-chauffeur/',
  dest: 'public',
  themeConfig: {
    navbar: false,
    sidebar: [
      '/',
      '/technologies',
      '/errors',
      '/setup',
      '/infrastructure',
      '/flux',
      '/rights',
      '/security',
    ]
  },
  locales: {
    '/': {
      lang: 'fr-FR',
    },
  },
};
