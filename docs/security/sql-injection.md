# Injections SQL

## Moteur de base de données

La base de données utilisée est MongoDB, et n'utilise pas de langage SQL.
En conséquence, on ne peut pas parler d'injection SQL.

## Injection noSQL

Il est cependant _possible_ de faire des injections noSQL, prenons pour référence 
l'article de [nullsweep (en anglais)](https://nullsweep.com/a-nosql-injection-primer-with-mongo/).

Les possibilités sont tout de même resteintes, en effet, il n'y est à priori pas possible de faire de 
l'écriture dans une requête de lecture, contrairement à l'aggrégation de requêtes SQL.

On notera cependant l'utilisation de [mongoose](https://mongoosejs.com/) sur e-Chauffeur, 
limitant pas mal les possibilités de mauvaise utilisation.
