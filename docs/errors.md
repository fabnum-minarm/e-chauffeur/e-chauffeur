# Codes d'erreurs

| Code | Message | Description |
|------|---------|-------------|
| 401 | Password expired | Le mot de passe de l'utilisateur est expiré |
| 403 | You're not authorized to * | L'utilisateur n'a pas les droits suffisants pour cette action |
| 404 | * Not found | L'entité recherchée est introuvable |
