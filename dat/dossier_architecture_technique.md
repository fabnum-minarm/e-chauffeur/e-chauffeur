![logos](static/logos.png)

# Dossier d'Architecture Technique -- ECHAUFFEUR

## Suivi du document

| Version | Auteur          | Modifications      | Date       |
|---------|-----------------|--------------------|------------|
| 1.0     | Charles Capelli | Rédaction initiale | 25/09/2022 |

## Fiche de contrôle du document

| Organisme                   | Nom             | Rôle                           | Activité  |
|-----------------------------|-----------------|--------------------------------|-----------|
| Fabrique numérique          | Charles Capelli | Développeur                    | Rédaction |
| Fabrique numérique          | Gyver Ferrand   | Administrateur SSI             | Relecture |
| Fabrique numérique          | Gaël Demette    | Architecte                     | Relecture |
| Fabrique numérique          | Robin Lebert    | Développeur                    | Relecture |
| Fabrique numérique          | ?               | Responsable Réalisation Projet | -         |
| Service Projet de la DIRISI | ?               | Responsable Conduite Projet    | Relecture |
| Service Projet de la DIRISI | ?               | Responsable SSI Projet         | Relecture |


## Présentation du dossier d’architecture

Ce dossier d’architecture a pour but de présenter les grandes lignes de l’architecture d'eChauffeur. Il n’est par conséquent ni un dossier d’installation, ni un dossier d’exploitation ou un dossier de spécifications fonctionnelles.

## Présentation du projet

Echauffeur doit permettre aux personnels du ministère de se déplacer d’un endroit à un autre et d’arriver à une heure prévue, quel que soit le soit le mode de déplacement utilisé. Le service répond au besoin de demander un transport plus ou moins longue distance avec chauffeur. Il s'est également enrichi en proposant la possibilité de planifier des trajets de navettes à intervals réguliers

## Architecture

### Stacks

La motivation dans le choix des technologies pour ce projet fut de choisir des technologies fiables, limitant le périmètre de connaissances techniques, afin de rester simple, de limiter la courbe d'apprentissage pour le personnel non familier (et faciliter les recrutements).

D'autre part, certaines technologies ont été sélectionnées afin de bénéficier de leur scalabilité, permettant de donner à la solution une haute disponibilité si désirée.

#### Backend
##### API

La web API est écrite en Javascript, et fonctionne sur NodeJS >= 12. Le souhait étant de bénéficier des modules ES6 sans le surcout d'un transpiler de code. (Ainsi que toutes les features ES6+ comme le async/await, etc...)

    Koa permet de décrire le routing de l'API, décrire le comportement CORS, ou encore parser le contenu JSON entrant.
    Socket IO permet de synchroniser facilement les sockets via Redis dans le cas où on souhaite avoir plus d'une instance de l'API en production.
    ESLint garanti le format de code.
    Mocha est utilisé afin de lancer les tests unitaires
    Mongoose est utilisé comme ORM pour faciliter l'exploitation de la base de données.

##### Base de données

Le système de base de données utilisé est MongoDB. Il s'agit d'une base de données no-SQL, dite schemaless, c'est-à-dire qu'elle ne requiert pas la définition d'un schema en amont.

Notre utilisation de MongoDB reste basique : écriture / lecture / aggrégation.

##### Redis

Un service redis est utilisé en sa qualité de pub/sub, son utilisation aujourd'hui se limite à synchroniser les websockets entre les différents pods d'API afin qu'une course émise sur un pod X soit bien reçue sur les clients connectés au pod Y.
#### Frontend
##### VueJS & NuxtJS

NuxtJS est utilisé pour cadrer l'organisation du code comme objectif premier.

Les trois dépots frontaux utilisent donc VueJS.

##### lib-vue

Il s'agit d'un dépot interne au projet ayant pour objectif de partager du code entre les différents frontaux, afin de faciliter la maintenance du projet.

#### Intégration & système
##### Docker

Docker est utilisé afin de construire des conteneurs Linux pouvant être exécutés localement, en préproduction et en production. Il est utilisé pour garantir l'idempotence de déploiement entre deux conteneurs linux.
##### Kubernetes

Kubernetes est utilisé en tant que système d'orchestration entre plusieurs machines physiques afin de construire un système redondé et fiable.

##### Gitlab CI

Gitlab est utilisé pour son système d'intégration continue / déploiement continue afin de vérifier le code, lancer les tests unitaires, construire et héberger les images docker, et orchestrer les déploiements automatiquers sur Kubernetes.

### Schéma de l'architecture

```mermaid
graph TD
    Dashboard[Client NuxtJS - régulateur] -->|https & wss| API(API)
    Driver[Client NuxtJS - chauffeur] -->|https & wss| API(API)
    Front[Client NuxtJS - utilisateur] -->|https & wss| API(API)
    API -->|tcp| Redis{Redis}
    API -->|tcp| MongoDB{MongoDB}
```

## Interopérabilité avec d’autres systèmes
### eChauffeur et son service SMTP 

eChauffeur utilise un service d'envoi de mail interne a l'API via la dépendance Nodemailer. Celle-ci permet d'envoyer des mails via SMTP depuis le code de l'API.

### Matrice des flux

| Source                | Sens  | Port  | Protocol | Sens  | Destination | Localisation                |
|-----------------------|-------|-------|----------|-------|-------------|-----------------------------|
| Site internet         | `>>>` | 443   | HTTPS    | `>>>` | API         | Cluster Igloo NS eChauffeur |
| Site internet         | `<<<` | 443   | HTTPS    | `<<<` | API         | Cluster Igloo NS eChauffeur |
| Dashboard             | `>>>` | 443   | HTTPS    | `>>>` | API         | Cluster Igloo NS eChauffeur |
| Dashboard             | `<<<` | 443   | HTTPS    | `<<<` | API         | Cluster Igloo NS eChauffeur |
| Application chauffeur | `>>>` | 443   | HTTPS    | `>>>` | API         | Cluster Igloo NS eChauffeur |
| Application chauffeur | `<<<` | 443   | HTTPS    | `<<<` | API         | Cluster Igloo NS eChauffeur |
| API                   | `>>>` | 27017 | TCP      | `>>>` | MongoDB     | Cluster Igloo NS eChauffeur |
| API                   | `<<<` | 1337  | TCP      | `<<<` | MongoDB     | Cluster Igloo NS eChauffeur |
| API                   | `>>>` | 6379  | TCP      | `>>>` | Redis       | Cluster Igloo NS eChauffeur |
| API                   | `<<<` | 1337  | TCP      | `<<<` | Redis       | Cluster Igloo NS eChauffeur |

<div style="page-break-after: always"></div>

## Exigences générales
### Accès aux serveurs et sécurité des échanges

L’ensemble des moyens mis en œuvre pour l’exploitation d'eChauffeur est hébergé sur la structure d’hébergement Igloo. Le portail est accessible par une URL déclarée dans un annuaire national sous contrôle de l'AFNIC.

L’architecture de l’application ne nécessite pas de prendre de disposition particulière pour la sécurité applicative sur les postes clients des utilisateurs. La navigation s’effectue à l’aide d’un navigateur type Firefox.

eChauffeur est composé d’un serveur NodeJS en interaction avec une base de données non exposée publiquement. Les 
échanges entre les 
postes client 
et le serveur 
sont réalisés avec le protocole HTTPS. Toute demande client se présentant sur le système par l’intermédiaire du protocole HTTP est automatiquement redirigée en HTTPS. Un certificat de sécurité sera mis en place pour le portail.

### Authentification, contrôle d’accès, habilitations et profils

Les utilisateurs sont identifiés de manière nominative par un token JWT généré par l'API. Celui est sécurisé par une 
durée de validité courte et une rotation d'un long secret généré aléatoirement à interval régulier. Le token 
embarque une partie des données utilisateurs qui ont été nettoyées de toute donnée sensible à l'émission par l'API.

L'ensemble des pages des services eChauffeur sont accessibles uniquement aux ayant droits. Ces droits ont été 
définis du plus restreint (le public) au plus permissif (le super administrateur) en compartimentant par métier 
(chauffeur, régulateur) et par base (un chauffeur de Toulon n'ayant pas accès à la base de Brest, par exemple).

Les profils fonctionnels sont modifiables uniquement par un utilisateur ayant un profil spécifique d’administration.

### Traçabilité des erreurs et des actions utilisateurs

La totalité des requêtes HTTP sont logués dans le stdout de l'API grâce à la dépendance dédiée Pino. Le niveau de 
log par défaut est `INFO`. L'ensemble des actions réalisables sur le service sont donc tracées.

### Sécurité

Les différents logiciels de base motorisant le portail suivent la politique de sécurité générale, notamment définie par le CALID et les éditeurs. Les patchs de sécurité seront déployés lorsque cela sera nécessaire, tant sur les moyens de développement que sur les moyens de production.
La mise en place d’un patch de sécurité suivra un processus de qualification s’assurant de la stabilité et de la non-régression. Le processus sera défini au cas par cas, entre toutes les parties et en fonction de l’impact potentiel.

Le retex des audits SSI des applications dont la Fabrique Numérique a la charge sera traduit en recommandations qui 
seront 
appliquées sur le projet.

Une veille technologique sur les recommandations de l’OWASP TOP 10 en matière de sécurité applicative est assurée par 
l’expert SSI de la Fabrique Numérique.

Pour finir, des scanners de vulnérabilité SAST et DAST s'exécutent à chaque nouveau commit sur le projet assure une 
veille supplémentaire en cas de nouveau développement ou de corrections.

### Disponibilité

Pas de contraintes particulières. Toutes les applications du service sont répliquées trois fois de sorte à assurer 
une haute disponibilité. Kubernetes assure le remontage des pods en défaut.
quotidiens.

### Intégrité

Une politique de sauvegarde (et de contrôle des sauvegardes) devra être mise en place afin de garantir qu’en cas de défaillance matérielle ou logicielle, la restauration des dernières données sauvegardées soit réalisable dans un délai acceptable (les préconisations devront être indiquées dans le dossier d’exploitation).

### Confidentialité

Les spécifications n’expriment pas le besoin de dispositions remarquables supplémentaires (par exemple le 
chiffrement de données) pour garantir le niveau de confidentialité des informations manipulées par le portail hors 
du contrôle des acces.
